Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-sdk
Source: https://invent.kde.org/plasma/plasma-sdk
Upstream-Contact: plasma-devel@kde.org

Files: *
Copyright: 2006-2008, Aaron Seigo <aseigo@kde.org>
           2018, Aleix Pol Gonzalez <aleixpol@kde.org>
           2020, Carl Schwan <carlschwan@kde.org>
           2019, Carson Black <uhhadd@gmail.com>
           2023, David Edmundson <davidedmundson@kde.org>
           2019-2020, David Redondo <kde@david-redondo.de>
           2013, Giorgos Tsiapaliokas <terietor@gmail.com>
           2020, Ismael Asensio <isma.af@gmail.com>
           2022-2023, ivan tkachenko <me@ratijas.tk>
           2012-2015, Marco Martin <mart@kde.org>
           2020, Nicolas Fella <nicolas.fella@gmx.de>
           2014-2017, Sebastian Kügler <sebas@kde.org>
License: GPL-2.0-or-later
Comment: Automatically extracted

Files: po/*
Copyright: 2007-2024, A S Alam <aalam@users.sf.net>
           2024, Adrián Chaves (Gallaecio)
           2019-2024, Alexander Yavorsky <kekcuha@gmail.com>
           2015-2023, Eloy Cuadra <ecuadra@eloihr.net>
           2022-2024, Emir SARI <emir_sari@icloud.com>
           2023, Enol P. <enolp@softastur.org>
           2015-2024, Freek de Kruijf <freekdekruijf@kde.nl>
           2022-2024, Kisaragi Hiu <mail@kisaragi-hiu.com>
           2017-2023, Kiszel Kristóf <kiszel.kristof@gmail.com>
           2021-2023, Kristóf Kiszel <kiszel.kristof@gmail.com>
           2022-2024, Mincho Kondarev <mkondarev@yahoo.de>
           2015-2023, Roman Paholik <wizzardsk@gmail.com>
           2015-2024, Stefan Asserhäll <stefan.asserhall@gmail.com>
           2015-2024, Steve Allewell <steve.allewell@gmail.com>
           2021-2024, Vincenzo Reale <smart2128vr@gmail.com>
           2015-2023, Vít Pelčák <vit@pelcak.org>
           2021-2024, Xavier Besnard <xavier.besnard@kde.org>
           2023, Yaron Shahrabani <sh.yaron@gmail.com>
           2012-2024, Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2009-2014, A Plasma fejlesztői
           2007-2018, Free Software Foundation
           2002-2017, Free Software Foundation, Inc
           2008-2009, K Desktop Environment
           2009, Rosetta Contributors and Canonical Ltd 2009
           2010, Rosetta Contributors and Canonical Ltd 2010
           2018, The Free Software Foundation, Inc
           2007-2016, This_file_is_part_of_KDE
           2006, กลุ่มผู้พัฒนา KDE
License: GPL-2.0-or-later
Comment: Half automatically extracted, half manually collected

Files: plasmoidviewer/main.cpp
Copyright: 2007-2008, Aaron Seigo <aseigo@kde.org>
           2007, Frerich Raabe <raabe@kde.org>
License: BSD-2-Clause

Files: .gitlab-ci.yml
       .kde-ci.yml
       engineexplorer/org.kde.plasmaengineexplorer.appdata.xml
       iconexplorer/org.kde.plasma.iconexplorer.appdata.xml
       plasmoidviewer/org.kde.plasmoidviewer.appdata.xml
Copyright: None
License: CC0-1.0

Files: lookandfeelexplorer/src/lnflistmodel.cpp
       lookandfeelexplorer/src/lnflistmodel.h
       themeexplorer/src/themelistmodel.cpp
       themeexplorer/src/themelistmodel.h
Copyright: 2002, Daniel Molkentin <molkentin@kde.org>
           2009, Davide Bettio <davide.bettio@kdemail.net>
           2007, Ivan Cukic <ivan.cukic+kde@gmail.com>
           2002, Karol Szwed <gallium@kde.org>
           2016, Marco Martin <mart@kde.org>
           2007, Paolo Capriotti <p.capriotti@gmail.com>
           2008, Petri Damsten <damu@iki.fi>
           2000, TrollTech AS.
           2007, Urs Wolfer <uwolfer@kde.org>
License: GPL-2.0-only

Files: engineexplorer/ktreeviewsearchline.cpp
       engineexplorer/ktreeviewsearchline.h
Copyright: 2006, Hamish Rodda <rodda@kde.org>
           2007, Pino Toscano <pino@kde.org>
           2005, Rafal Rzepecki <divide@users.sourceforge.net>
           2003, Scott Wheeler <wheeler@kde.org>
License: LGPL-2.0-only

Files: iconexplorer/autotests/iconmodeltest.cpp
       lookandfeelexplorer/package/contents/ui/FormField.qml
       lookandfeelexplorer/package/contents/ui/main.qml
       lookandfeelexplorer/package/contents/ui/MetadataEditor.qml
       lookandfeelexplorer/src/lnflogic.cpp
       lookandfeelexplorer/src/lnflogic.h
       lookandfeelexplorer/src/main.cpp
       plasmoidviewer/qmlpackages/shell/contents/configuration/AboutPlugin.qml
       plasmoidviewer/qmlpackages/shell/contents/views/FloatingToolBar.qml
       plasmoidviewer/qmlpackages/shell/contents/views/SdkButtons.qml
       themeexplorer/package/contents/ui/ColorButton.qml
       themeexplorer/package/contents/ui/ColorEditor.qml
       themeexplorer/package/contents/ui/delegates/actionbutton.qml
       themeexplorer/package/contents/ui/delegates/allframesvgs.qml
       themeexplorer/package/contents/ui/delegates/analog_meter.qml
       themeexplorer/package/contents/ui/delegates/busyindicator.qml
       themeexplorer/package/contents/ui/delegates/button.qml
       themeexplorer/package/contents/ui/delegates/checkmarks.qml
       themeexplorer/package/contents/ui/delegates/clock.qml
       themeexplorer/package/contents/ui/delegates/containment-controls.qml
       themeexplorer/package/contents/ui/delegates/dialog.qml
       themeexplorer/package/contents/ui/delegates/framesvg.qml
       themeexplorer/package/contents/ui/delegates/Hand.qml
       themeexplorer/package/contents/ui/delegates/icons.qml
       themeexplorer/package/contents/ui/delegates/listitem.qml
       themeexplorer/package/contents/ui/delegates/monitor.qml
       themeexplorer/package/contents/ui/delegates/panel.qml
       themeexplorer/package/contents/ui/delegates/progressbar.qml
       themeexplorer/package/contents/ui/delegates/scrollbar.qml
       themeexplorer/package/contents/ui/delegates/slider.qml
       themeexplorer/package/contents/ui/delegates/tabbar.qml
       themeexplorer/package/contents/ui/delegates/textfield.qml
       themeexplorer/package/contents/ui/fakecontrols/Button.qml
       themeexplorer/package/contents/ui/fakecontrols/CheckBox.qml
       themeexplorer/package/contents/ui/fakecontrols/LineEdit.qml
       themeexplorer/package/contents/ui/FormLabel.qml
       themeexplorer/package/contents/ui/main.qml
       themeexplorer/package/contents/ui/MetadataEditor.qml
       themeexplorer/src/coloreditor.cpp
       themeexplorer/src/coloreditor.h
       themeexplorer/src/main.cpp
       themeexplorer/src/thememodel.cpp
       themeexplorer/src/thememodel.h
Copyright: 2018, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2014, Alex Richardson <arichardson.kde@gmail.com>
           2013, Antonis Tsiapaliokas <kok3rs@gmail.com>
           2013, David Edmundson <davidedmundson@kde.org>
           2020, David Redondo <kde@david-redondo.de>
           2023, ivan tkachenko <me@ratijas.tk>
           2012-2016, Marco Martin <mart@kde.org>
           2012, Viranch Mehta <viranch.mehta@gmail.com>
License: LGPL-2.0-or-later

Files: po/ca/*
       po/ca@valencia/*
       po/uk/*
Copyright: 2015-2020, Antoni Bella Pérez <antonibella5@yahoo.com>
           2019, Empar Montoro Martín <montoro_mde@gva.es>
           2007-2024, Josep M. Ferrer <txemaq@gmail.com>
           2007-2008, Ivan Petrouchtchak <fr.ivan@ukrainian-orthodox.org>
           2008-2023, Yuri Chornoivan <yurchor@ukr.net>
License: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

Files: debian/*
Copyright: 2022-2024, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2015, Harald Sitter <sitter@kde.org>
           2022, Patrick Franz <deltaone@debian.org>
License: GPL-2.0-or-later

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GPL-2.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: LGPL-2.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version
 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.0-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option)  any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 2.1 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.

License: LGPL-3.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 3 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in
 `/usr/share/common-licenses/LGPL-3’.

License: LicenseRef-KDE-Accepted-LGPL
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 license or (at your option) any later version that is accepted by
 the membership of KDE e.V. (or its successor approved by the
 membership of KDE e.V.), which shall act as a proxy as defined in
 Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
